# -*- coding: utf-8 -*-
"""
Created on Wed Feb 12 15:11:07 2020

@author: Brian
"""

import socket
import sys
import os

def client():
    
    #Load in the hostnames to get
    f = open(os.path.join(os.path.dirname(__file__), "PROJ2-HNS.txt"), "r")

    #Create output file
    outFile = open(os.path.join(os.path.dirname(__file__), "RESOLVED.txt"), "w")
    
    #For each hostname, try to get it from the server
    for l in f:
        query = l.strip()
        print("Query: {}".format(query))
        try:
            cs_rs = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        except socket.error as err:
            print('socket open error: {} \n'.format(err))
            exit()
            
        # Define the address and port of the LS server
        lsPort = int(sys.argv[2])
        lsHost_addr = socket.gethostbyname(sys.argv[1])
    
        # Connect to the LS server
        server_binding = (lsHost_addr, lsPort)
        cs_rs.connect(server_binding)
        cs_rs.send(query.encode('utf-8'))
        
        # Receive data from the server
        data_from_server=cs_rs.recv(100)
        received = data_from_server.decode('utf-8')
        
        cs_rs.close()
        
        print('Result: {} \n'.format(received))
        outFile.write("{}\n".format(received))
        print("-------------------------------------------------------------")
    f.close()
    outFile.close()
    
    

    #exit()
    
client()