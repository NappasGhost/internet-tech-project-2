# -*- coding: utf-8 -*-
"""
Created on Wed Feb 12 14:39:01 2020

@author: Brian
"""

import socket
import sys
import select

def run():
    #Create the socket
    try:
        ss = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        print("[S]: Server socket created")
    except socket.error as err:
        print('socket open error: {}\n'.format(err))
        exit()

    server_binding = ('', int(sys.argv[1]))
    ss.bind(server_binding)
    host = socket.gethostname()
    print("[S]: Server host name is {}".format(host))
    localhost_ip = (socket.gethostbyname(host))
    print("[S]: Server IP address is {}".format(localhost_ip))
    
    running = True
    while running:
        #Listen for a client connection
        ss.listen(1)
        csockid, addr = ss.accept()
        print ("[S]: Got a connection request from a client at {}".format(addr))
        
        #Get the data sent from the client
        data_from_client=csockid.recv(100)
        lookup_hostname = data_from_client.decode('utf-8')
        print("[C]: Data received from client: {}".format(lookup_hostname))

        #Send data from client to both top-level servers    
        sock_ts1 = createTS(sys.argv[2], int(sys.argv[3]))
        sock_ts2 = createTS(sys.argv[4], int(sys.argv[5]))
        
        sock_ts1.send(data_from_client)
        sock_ts2.send(data_from_client)
        
        #Check which TS server responded within a 5 second timeout
        buffer, _IGNORE1, _IGNORE2 = select.select([sock_ts1, sock_ts2], [], [], 5)
        
        #Respond to the client
        if len(buffer) > 0:
            csockid.send(buffer[0].recv(100))
        else:
            csockid.send((lookup_hostname + " - Hostname not found").encode('utf-8'))

    # Close the server socket
    ss.close()
    #exit()
    
def createTS(hostname, port):    
    try:
        sock_ts = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    except socket.error as err:
        print('socket open error: {} \n'.format(err))
        exit()
    
    # Connect to the LS server
    server_binding = (hostname, port)
    sock_ts.connect(server_binding)
    
    return sock_ts
    

run()

    