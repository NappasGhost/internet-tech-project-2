import socket
import os
import sys

def server():
    #Create the socket
    try:
        ss = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        print("[S]: Server socket created")
    except socket.error as err:
        print('socket open error: {}\n'.format(err))
        exit()

    server_binding = ('', int(sys.argv[1]))
    ss.bind(server_binding)
    host = socket.gethostname()
    print("[S]: Server host name is {}".format(host))
    localhost_ip = (socket.gethostbyname(host))
    print("[S]: Server IP address is {}".format(localhost_ip))
    
    #Load in the hostname to address pairs from file
    f = open(os.path.join(os.path.dirname(__file__), "PROJ2-DNSTS1.txt"), "r")
    dnsTable = {}
    
    #Load those into a dictionary
    for l in f:
        fieldArr = l.split()
        dnsTable.update({fieldArr[0].lower() : (fieldArr[1],fieldArr[2])})
    
    f.close()
    
    running = True
    while running:
        #Listen for a load balancing connection
        ss.listen(1)
        lssockid, addr = ss.accept()
        print ("[S]: Got a connection request from a client at {}".format(addr))
        
        #Get the data sent from the load balancing server
        data_from_client=lssockid.recv(100)
        print("[C]: Data received from client: {}".format(data_from_client.decode('utf-8')))    
        
        #Respond to the load balancing server
        hostname = data_from_client.decode('utf-8').lower()
        result = dnsTable.get(hostname)
        if result:
            lssockid.send((hostname + " " + result[0] + " " + result[1]).encode('utf-8'))

    # Close the server socket
    ss.close()
    #exit()
    
    
server()


